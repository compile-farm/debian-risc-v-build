# Kernel RISC-V build for VisionFive 2 for cfarm

Automated scripts to build Debian kernel for RISC-V boards used on <https://portal.cfarm.net>

We want to stay as close to upstream as possible and build kernels compatible with Debian.

This repository builds kernels for VisionFive2 boards.

## Important note

**This kernel build is now deprecated: the 6.11 kernel in Debian fully supports the VisionFive 2 board, including PCIe/NVMe support.**

As of October 2024, the 6.11 kernel is available from the Debian experimental repository: <https://packages.debian.org/experimental/linux-image-riscv64>

## Automated kernel build

Everything is done in [Gitlab CI](.gitlab-ci.yml).  We use:

- the `JH7110_VisionFive2_upstream` branch from <https://github.com/starfive-tech/linux/>
- the [Debian kernel config for riscv64](config-riscv64-debian), taken from [linux-image-riscv64](https://packages.debian.org/sid/linux-image-riscv64) (currently 6.6.11-1-riscv64)
- our [local kernel config addition](config-riscv64-local) that we merge with the Debian config.  It contains kernel config for the Starfive patches that are not yet upstream as well as enabling drivers required on the VisionFive2.

As more VisionFive2 patches get merged in Linux upstream, our goal is to progressively reduce the amount
of local kernel config by progressively updating to the latest Debian kernels.  At some point we will be able
to use the Debian kernel directly.

## Downloadable kernel packages

- [Permanent link to latest artifacts](https://framagit.org/compile-farm/debian-risc-v-build/-/jobs/artifacts/main/browse?job=kernel) (possibly untested)
- [6.6.0 kernel package](https://framagit.org/compile-farm/debian-risc-v-build/-/jobs/2231792/artifacts/browse) based on [13eb70da2a73187c8c7aece13d23d68928aa8210](https://github.com/starfive-tech/linux/commits/13eb70da2a73187c8c7aece13d23d68928aa8210) (tested)
- [6.6.0 kernel package](https://framagit.org/compile-farm/debian-risc-v-build/-/jobs/2228702/artifacts/browse) based on [9fe004eaf1aa5b23bd5d03b4cfe9c3858bd884c4](https://github.com/starfive-tech/linux/commits/9fe004eaf1aa5b23bd5d03b4cfe9c3858bd884c4) (tested)
- [6.5.0 kernel package](https://framagit.org/compile-farm/debian-risc-v-build/-/jobs/2117890/artifacts/browse) based on [4e3f341e200d01ece4f8acb18a0300b13eda2489](https://github.com/starfive-tech/linux/commits/4e3f341e200d01ece4f8acb18a0300b13eda2489) (tested)

## Current progress

| Status | Subsystem | Details |
|-|----|-|
| ✅ | UART | Serial console is working |
| ✅ | eMMC | Boots OK from eMMC |
| ✅ | microSD | OK |
| ✅ | Internal SPI NOR flash | OK (only tested reading) |
| ✅ | USB | Tested multiple peripherical (storage, keyboard...) |
| ❗✅ | Ethernet | OK after building kernel with `CONFIG_MOTORCOMM_PHY` |
| ❗✅ | CPU frequency scaling | OK but stays at maximum freq |
| ❗ | Memory | Only 4 GB out of 8 GB is visible (with stock u-boot)
| ❗✅ | NVMe | Requires non-upstream patches for now, and is very sensitive to the power supply |
| ❌ | Reboot | Rebooting is very unreliable |
| ❌ | Crypto module | Causes lots of backtraces |
| ❓ | Display | Not tested |

### Ethernet

The Debian kernel config for riscv64 did not enable `CONFIG_MOTORCOMM_PHY` until version 6.6.7-1~exp1. It is required for the network interfaces to work: without it, the interfaces will appear to go up from the system, but no traffic will flow (or only outgoing traffic).

An alternative is to run a DHCP client from u-boot using the "dhcp" command. This will setup the PHY beforehand so that the Linux kernel does not need to do it later (thanks to Jonathan McDowell for the hint).

Debian kernels since 6.6.7-1~exp1 include this module. Networking should now work fine.

### CPU frequency scaling

It works, with 4 frequency steps:

```
# cpupower frequency-info
analyzing CPU 1:
  driver: cpufreq-dt
  CPUs which run at the same hardware frequency: 0 1 2 3
  CPUs which need to have their frequency coordinated by software: 0 1 2 3
  maximum transition latency:  Cannot determine or is not supported.
  hardware limits: 375 MHz - 1.50 GHz
  available frequency steps:  375 MHz, 500 MHz, 750 MHz, 1.50 GHz
  available cpufreq governors: performance schedutil
  current policy: frequency should be within 375 MHz and 1.50 GHz.
                  The governor "performance" may decide which speed to use
                  within this range.
  current CPU frequency: 1.50 GHz (asserted by call to hardware)
```

However, the frequency seems to be stuck to 1.50 GHz.  Forcing the `powersave` governor correctly sets the frequency to 375 MHz, which reduces power consumption from 6 W to 5 W (rough figures, measured on the AC side before a cheap power brick).

### Memory

Only 4 GB is visible on the system, while the board is supposed to have 8 GB.

According to [Alpine](https://gitlab.alpinelinux.org/nmeum/alpine-visionfive/-/commit/5368f97039db22da0a7ef18c1ee230fabb9523b7) it's a u-boot issue, more recent versions of u-boot detect the memory automatically.

### NVMe

NVMe requires PCIe support, and this is not yet upstream in Linux, [see Starfive upstream status](https://rvspace.org/en/project/JH7110_Upstream_Plan). It works because we use Starfive patches.

A Crucial P3 drive works correctly.

However, a Samsung 970 EVO Plus drive fails spectacularly. The system completely locks up shortly after boot, with RCU stalls and hard lockup printed to the serial console:

```
[   34.799589] rcu: INFO: rcu_sched detected stalls on CPUs/tasks:
[   34.805520] rcu:     3-...0: (10 ticks this GP) idle=d754/1/0x4000000000000000 softirq=2411/2411 fqs=1053
[   34.814830] rcu:     (detected by 2, t=5256 jiffies, g=1493, q=1059 ncpus=4)
[   34.821618] Task dump for CPU 3:
[   34.824849] task:(udev-worker)   state:R  running task     stack:0     pid:274   ppid:261    flags:0x0000000a
[   34.834769] Call Trace:
[   34.837220] [<ffffffff80944056>] __schedule+0x346/0xb0e

[   48.279586] watchdog: Watchdog detected hard LOCKUP on cpu 3

[   35.889856] rcu:     0-...!: (0 ticks this GP) idle=47cc/1/0x4000000000000004 softirq=2624/2624 fqs=0
[   35.898816] rcu:     (detected by 2, t=5255 jiffies, g=1765, q=495 ncpus=4)
[   35.905515] Task dump for CPU 0:
[   35.908743] task:swapper/0       state:R  running task     stack:0     pid:0     ppid:0      flags:0x00000000
[   35.918658] Call Trace:
[   35.921105] [<ffffffff80944056>] __schedule+0x346/0xb0e
[   35.926341] [<ffffffff8094200c>] default_idle_call+0x2e/0xd0
[   35.932003] rcu: rcu_sched kthread timer wakeup didn't happen for 5264 jiffies! g1765 f0x0 RCU_GP_WAIT_FQS(5) ->state=0x402
[   35.943125] rcu:     Possible timer handling issue on cpu=0 timer-softirq=549
[   35.949995] rcu: rcu_sched kthread starved for 5270 jiffies! g1765 f0x0 RCU_GP_WAIT_FQS(5) ->state=0x402 ->cpu=0
[   35.960163] rcu:     Unless rcu_sched kthread gets sufficient CPU time, OOM is now expected behavior.
[   35.969113] rcu: RCU grace-period kthread stack dump:
[   35.974160] task:rcu_sched       state:I stack:0     pid:15    ppid:2      flags:0x00000000
[   35.982511] Call Trace:
[   35.984957] [<ffffffff80944056>] __schedule+0x346/0xb0e
[   35.990186] [<ffffffff8094486c>] schedule+0x4e/0xce
[   35.995067] [<ffffffff8094a3d8>] schedule_timeout+0x8a/0x152
[   36.000728] [<ffffffff800a392e>] rcu_gp_fqs_loop+0x2fc/0x3c8
[   36.006394] [<ffffffff800a8880>] rcu_gp_kthread+0x114/0x156
[   36.011969] [<ffffffff8003f306>] kthread+0xc4/0xe4
[   36.016764] [<ffffffff80003e3e>] ret_from_fork+0xe/0x20
[   36.021994] rcu: Stack dump where RCU GP kthread last ran:
[   36.027477] Task dump for CPU 0:
[   36.030706] task:swapper/0       state:R  running task     stack:0     pid:0     ppid:0      flags:0x00000000
[   36.040623] Call Trace:
[   36.043072] [<ffffffff80944056>] __schedule+0x346/0xb0e
[   36.048301] [<ffffffff8094200c>] default_idle_call+0x2e/0xd0
[   48.279928] watchdog: Watchdog detected hard LOCKUP on cpu 0
[   48.285605] Modules linked in: ccm nls_ascii jh7110_pwmdac jh7110_tdm snd_soc_spdif_tx ofpart nls_cp437 vfat fat snd_soc_core spi_nor starfive_wdt jh7110_crypto(+) jh7110_trng mtd snd_pcm_dmaengine watchdog snd_pcm vs_drm pwm_starfive_ptc sfctemp crypto_engine snd_timer drm_dma_helper snd drm_kms_helper soundcore cpufreq_dt drm fuse loop drm_panel_orientation_quirks dm_mod dax configfs ip_tables x_tables autofs4 ext4 crc32c_generic crc16 mbcache jbd2 mmc_block nvme xhci_pci xhci_hcd nvme_core t10_pi crc64_rocksoft crc64 crc_t10dif crct10dif_generic crct10dif_common motorcomm axp20x_regulator usbcore axp20x_i2c axp20x mfd_core regmap_i2c dwmac_starfive stmmac_platform usb_common stmmac dw_mmc_starfive dw_mmc_pltfm pcs_xpcs of_mdio fixed_phy dw_mmc phy_jh7110_usb clk_starfive_jh7110_aon phylink clk_starfive_jh7110_vout fwnode_mdio clk_starfive_jh7110_isp phy_jh7110_dphy_rx libphy clk_starfive_jh7110_stg mmc_core spi_cadence_quadspi phy_jh7110_pcie i2c_designware_platform spi_pl022 i2c_designware_core
```

After some trial and error, including testing several identical NVMe
drives to rule out a hardware failure, the problem seems highly related to
the power supply. Testing several USB-C power supplies gives the following
results with a Samsung 970 EVO Plus drive:

- Akashi smart charger 5V/2.4A using 230V, model ALT2USBACCH: kernel stalls
  as soon as the NVMe drive is present, even if not using it at all
- ALLNET USB charger 5V/3A using 230V, model KA1803A-EU: works fine when the
  NVMe drive is present and unused, but produces kernel stalls when trying
  to actually read/write data from the NVMe
- OnePlus power supply 5V/2A or 5V/6A in WarpCharge mode, using 230V,
  model WC0506A3HK: works correctly in all cases, even when actively using
  the NVMe

In all cases, the measured average power (measured with a smart plug on
the 230 V AC side) is less than 8 W, so we should not be hitting the
maximum power output of the power supplies. However, they might degrade
the voltage under load and cause the NVMe to malfunction, or maybe the
NVMe requires too much power for very short periods of time.

The Crucial P3 drive works correctly with all power supplies, which means
that it either draws less power than the Samsung 970 EVO Plus, or that it
is much less sensitive to the quality of the power signal.

In the end, either the board or the NVMe drive is very sensitive to the
quality and power rating of the power supply for correct operation. It is
unknown whether the VisionFive2 hardware or the kernel drivers could be
improved to mitigate this issue.

It should be noted that the development kits provided by RISC-V
International include a high-quality 5V/6A power supply, but unfortunately
this power supply is not physically compatible with EU electrical plugs.

### Rebooting

Trying to reboot from Linux doesn't work reliably. Sometimes it "freezes"
the serial console and leaves the board in a weird state: it can still
ping, but SSH is not working at all (maybe the main storage gets disabled
too soon). Sometimes it goes further and systemd shuts everything down
correctly, but then the board never comes back up, it needs a power
cycle. Sometimes (but very rarely), rebooting works.

Also tested with u-boot 2024.1, the issue is still here.

This could be a kernel or userspace (systemd) issue.

### Crypto module

The `jh7110_crypto` module seems broken, there are lots of backtraces and hung tasks such as:

```
WARNING: CPU: 2 PID: 265 at crypto/api.c:172 crypto_wait_for_test+0x8a/0x92

INFO: task cryptomgr_test:327 blocked for more than 120 seconds.
```

[Alpine disabled this module as well](https://gitlab.alpinelinux.org/nmeum/alpine-visionfive/-/commit/03fbaff61db5702d50388dd0bfeddede958eb597)

Full backtrace:

```
[  195.245072] ------------[ cut here ]------------
[  195.245077] WARNING: CPU: 0 PID: 278 at crypto/api.c:176 crypto_wait_for_test+0x8e/0x92
[  195.245097] Modules linked in: nls_ascii nls_cp437 vfat fat snd_soc_spdif_tx jh7110_tdm jh7110_pwmdac ofpart ccm snd_soc_core vs_drm snd_pcm_dmaengine
[  195.245128] systemd-udevd[257]: 16000000.crypto: Worker [278] processing SEQNUM=1725 killed
[  195.245129]  drm_dma_helper spi_nor snd_pcm jh7110_trng mtd snd_timer drm_kms_helper jh7110_crypto(+) starfive_wdt watchdog crypto_engine snd pwm_starfive_ptc sfctemp soundcore cpufreq_dt drm loop fuse dm_mod drm_panel_orientation_quirks dax configfs ip_tables x_tables autofs4 ext4 crc32c_generic crc16 mbcache jbd2 mmc_block xhci_pci xhci_hcd motorcomm usbcore axp20x_regulator axp20x_i2c dwmac_starfive axp20x stmmac_platform mfd_core usb_common regmap_i2c stmmac dw_mmc_starfive dw_mmc_pltfm clk_starfive_jh7110_vout dw_mmc pcs_xpcs of_mdio phy_jh7110_dphy_rx clk_starfive_jh7110_isp mmc_core fixed_phy phylink fwnode_mdio clk_starfive_jh7110_aon libphy spi_cadence_quadspi clk_starfive_jh7110_stg phy_jh7110_pcie phy_jh7110_usb i2c_designware_platform i2c_designware_core spi_pl022
[  195.245282] CPU: 0 PID: 278 Comm: (udev-worker) Not tainted 6.5.0-starfive2 #1
[  195.245288] Hardware name: StarFive VisionFive 2 v1.3B (DT)
[  195.245292] epc : crypto_wait_for_test+0x8e/0x92
[  195.245299]  ra : crypto_wait_for_test+0x44/0x92
[  195.245306] epc : ffffffff803da8ce ra : ffffffff803da884 sp : ffffffc80082b830
[  195.245310]  gp : ffffffff8158f178 tp : ffffffd8c8f8c4c0 t0 : 0000000000000000
[  195.245314]  t1 : 0000000000006000 t2 : 000000000000067f s0 : ffffffc80082b850
[  195.245318]  s1 : ffffffd8c08cb200 a0 : fffffffffffffe00 a1 : ffffffc80082b798
[  195.245321]  a2 : ffffffc80082b798 a3 : ffffffd8c08cb398 a4 : 0000000000000000
[  195.245325]  a5 : fffffffffffffe00 a6 : 0000000000000001 a7 : 0000000000000000
[  195.245328]  s2 : ffffffff01d30440 s3 : 0000000000000000 s4 : ffffffff01d30380
[  195.245332]  s5 : 000000000000000a s6 : ffffffff80e50210 s7 : ffffffff01d30280
[  195.245336]  s8 : ffffffff815d0ca8 s9 : 0000000000000000 s10: ffffffc80082bca0
[  195.245339]  s11: ffffffff01d329c0 t3 : ffffffff81592430 t4 : 00000000000003ff
[  195.245343]  t5 : 00000000ffffffff t6 : 0000000000000003
[  195.245346] status: 0000000200000120 badaddr: 0000000000000000 cause: 0000000000000003
[  195.245351] [<ffffffff803da8ce>] crypto_wait_for_test+0x8e/0x92
[  195.245359] [<ffffffff803dc9a8>] crypto_register_alg+0xa8/0xea
[  195.245367] [<ffffffff803dfacc>] crypto_register_ahashes+0x4c/0xd0
[  195.245377] [<ffffffff01d2b9b0>] starfive_hash_register_algs+0x22/0x2a [jh7110_crypto]
[  195.245418] [<ffffffff01d2a6a8>] starfive_cryp_probe+0x41c/0x4bc [jh7110_crypto]
[  195.245446] [<ffffffff806599e6>] platform_probe+0x5e/0xba
[  195.245455] [<ffffffff80656a04>] really_probe+0xa0/0x35a
[  195.245464] [<ffffffff80656d38>] __driver_probe_device+0x7a/0x138
[  195.245471] [<ffffffff80656e2e>] driver_probe_device+0x38/0xc6
[  195.245479] [<ffffffff80657070>] __driver_attach+0xd0/0x1b8
[  195.245486] [<ffffffff8065459e>] bus_for_each_dev+0x6c/0xba
[  195.245494] [<ffffffff8065629a>] driver_attach+0x26/0x2e
[  195.245501] [<ffffffff80655a98>] bus_add_driver+0x108/0x20a
[  195.245508] [<ffffffff80658162>] driver_register+0x52/0xf4
[  195.245516] [<ffffffff806595d2>] __platform_driver_register+0x28/0x30
[  195.245522] [<ffffffff01d3c028>] starfive_cryp_driver_init+0x28/0x1000 [jh7110_crypto]
[  195.245552] [<ffffffff800028be>] do_one_initcall+0x5c/0x22e
[  195.245560] [<ffffffff800b3cea>] do_init_module+0x5e/0x216
[  195.245567] [<ffffffff800b593a>] load_module+0x17c6/0x1d1e
[  195.245573] [<ffffffff800b608a>] init_module_from_file+0x82/0xba
[  195.245579] [<ffffffff800b6276>] sys_finit_module+0x194/0x326
[  195.245585] [<ffffffff80940f9c>] do_trap_ecall_u+0xc6/0x134
[  195.245595] [<ffffffff80003cec>] ret_from_exception+0x0/0x64
[  195.245604] ---[ end trace 0000000000000000 ]---
```


## Future work

- Automate generation of a complete image (standard Debian rootfs + kernel + u-boot)
- Look at how Ubuntu is managing u-boot + EFI + grub: https://wiki.ubuntu.com/RISC-V/StarFive%20VisionFive%202
